### Project symfony de A à Z

## Pré-requis

- PHP 7.4
- Composer
- Symfony CLI
- Docker
- Docker-compose
- yarn /npm
- nodejs

```bash
symfony check:requirements
```

## Lancer l'environement de développement
```bash
composer install
yarn install / npm install
yarn dev / npm run build
docker-compose up -d
symfony serve
```

## Technologie utilisé:

- Docker
- Bootstrap
- GitLab CI
-> Pipeline d'integration continue

## Lancement des tests unitaires

```bash
php bin/phpunit --testdox
```


## Mise en place

- symfony console make:docker:database; -> mysql -> latest -> database
-> Creation du fichier docker-compose.yml avec mysql dedans -> nom de la base de donnée "main" sur le port 3306
- Installation de mailcacher pour tester les mails dans le fichier docker-compose.yml

- Lançage des containers -> docker-compose up -d
- symfony serve (-d)

- Creation des Unit Tests pour l'entité User (symfony console make:unit-test)
- Commande pour lancer les tests -> php bin/phpunit --testdox

- Installation de Webpack encore -> composer require symfony/webpack-encore-bundle
- yarn install
- Installer sass loader -> yarn add sass-loader@^12.0.0 sass --dev
- Dans le fichier webpack il faut activer .enableSassLoader()
- Installation de postcss loader auto prefix -> yarn add postcss-loader autoprefixer --dev
- Creation du fichier postcss.config.js
-> avec ces lignes

```
module.exports = {
    plugins: {
        autoprefixer: {}
    }
}
```

- Installation de Bootstrap 5 -> yarn add bootstrap@next
- Installation de popperjs/core -> yarn add @popperjs/core
- Modifier le app.js pour lui dire d'utiliser bootstrap
```
import { Tooltip, Toast, Popover } from 'bootstrap';
```
- Dans app.scss il faut importer bootstrap -> @import "~bootstrap/scss/bootstrap";

- bouger "postcss-loader": "^6.2.1" qui est dans "devDependencies" pour le mettre dans "dependencies"

- Nous pouvons installer un theme bootstrap ex avec Landkit

- yarn install / npm install
- yarn dev / npm build

Note: (follow episode 4 for next.)

Pour loader les images et les appeler plus simplement mise en place d'un code dans le fichier app.js

```
const imagesContext = require.context('../../../assets/images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);
```


