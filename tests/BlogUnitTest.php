<?php

namespace App\Tests;

use App\Entity\Blog;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class BlogUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $blog = new Blog();
        $user = new User();

        $blog->setName('name')
             ->setDescription('description')
             ->setUser($user)
        ;

        $this->assertTrue($blog->getName() === 'name');
        $this->assertTrue($blog->getDescription() === 'description');
        $this->assertTrue($blog->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $blog = new Blog();
        $user = new User();

        $blog->setName('name')
             ->setDescription('description')
             ->setUser($user)
        ;

        $this->assertFalse($blog->getName() === 'false');
        $this->assertFalse($blog->getDescription() === 'false');
        $this->assertFalse($blog->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $blog = new Blog();

        $this->assertEmpty($blog->getName());
        $this->assertEmpty($blog->getDescription());
        $this->assertEmpty($blog->getUser());

    }
}
