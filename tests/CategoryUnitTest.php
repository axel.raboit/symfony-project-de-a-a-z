<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $category = new Category();

        $category->setName('name')
             ->setDescription('description')
        ;

        $this->assertTrue($category->getName() === 'name');
        $this->assertTrue($category->getDescription() === 'description');
    }

    public function testIsFalse(): void
    {
        $category = new Category();

        $category->setName('name')
             ->setDescription('description')
        ;

        $this->assertFalse($category->getName() === 'false');
        $this->assertFalse($category->getDescription() === 'false');
    }

    public function testIsEmpty(): void
    {
        $category = new Category();

        $this->assertEmpty($category->getName());
        $this->assertEmpty($category->getDescription());
    }
}
